const utils = require("./utils");
const Archiotect = require("../lib/archiotect");
const path = require("path");

describe("Load a core plugin", () =>
{
    test("Normal", () =>
    {
        var events = [];
        var obj;
        var module_name = path.resolve(__dirname, "./ressources/plugin4/archiotect.js");
        obj = new Archiotect([
            {
                module_name
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(2);
                utils.check_event(events, 0, "loading", module_name, false);
                utils.check_event(events, 1, "loaded",
                {
                    exports: "plugin4",
                    module_name,
                    core: true,
                    name: "plugin4",
                    version: "1.0.0",
                    need: [],
                    load: undefined,
                    handle: undefined
                }, false);
            });
    });

    test.each(["pre_load", "post_load"])("Preventing the load of a normal plugin : %s", (type) =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, `./ressources/plugin${type === "pre_load" ? 4 : 5}/archiotect.js`));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(4);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "loading", module_name[1], false);
                utils.check_event(events, 2, "loaded",
                {
                    exports: `plugin${type === "pre_load" ? 4 : 5}`,
                    module_name: module_name[1],
                    core: true,
                    name: `plugin${type === "pre_load" ? 4 : 5}`,
                    version: "1.0.0",
                    need: [],
                    load: undefined,
                    handle: undefined
                }, false);
                utils.check_event(events, 3, "skipped", module_name[0], true);
            })
    });

    test.each(["pre_load", "post_load"])("Allowing the load of a normal plugin : %s", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, `./ressources/plugin6/archiotect.js`));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(4);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "loading", module_name[1], false);
                utils.check_event(events, 2, "loaded",
                {
                    exports: `plugin6`,
                    module_name: module_name[1],
                    core: true,
                    name: `plugin6`,
                    version: "1.0.0",
                    need: [],
                    load: undefined,
                    handle: undefined
                }, false);
                utils.check_event(events, 3, "loaded", {
                    exports: `plugin2`,
                    module_name: module_name[0],
                    core: false,
                    name: `plugin2`,
                    version: "1.0.0",
                    need: [],
                    load: undefined,
                }, false);
            })
    });

    test("Loading 2 core plugin and a normal one", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, `./ressources/plugin6/archiotect.js`));
        module_name.push(path.resolve(__dirname, `./ressources/plugin5/archiotect.js`));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        },
        {
            module_name: module_name[2]
    }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(6);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "loading", module_name[1], false);
                utils.check_event(events, 2, "loading", module_name[2], false);
                utils.check_event(events, 3, "loaded",
                {
                    exports: `plugin5`,
                    module_name: module_name[2],
                    core: true,
                    name: `plugin5`,
                    version: "1.0.0",
                    need: [],
                    load: undefined,
                    handle: undefined
                }, false);
                utils.check_event(events, 4, "loaded", {
                    exports: `plugin6`,
                    module_name: module_name[1],
                    core: true,
                    name: `plugin6`,
                    version: "1.0.0",
                    need: [],
                    load: undefined,
                    handle: undefined
                }, false);
                utils.check_event(events, 5, "skipped",module_name[0], true);
            })
    });
})