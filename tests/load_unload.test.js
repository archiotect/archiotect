const utils = require("./utils");
const Archiotect = require("../lib/archiotect");
const path = require("path");

describe("Empty/Bad Archiotect", () =>
{
    test("Normal", () =>
    {
        var events = [];
        var obj;

        obj = new Archiotect();
        utils.setup_event(obj, events);
        expect(events.length)
            .toBe(0);
    });

    test("Wrong type", () =>
    {
        var events = [];

        expect(() =>
            {
                new Archiotect("toto")
            })
            .toThrow();
    });

    test("Bad param", () =>
    {
        var events = [];
        var obj;
        var module_name = path.resolve(__dirname, "./ressources/plugin01/archiotect.js");
        
        obj = new Archiotect([
            {
                module_name: module_name
            }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then((res) =>
            {
                expect(res)
                    .toEqual([]);
                expect(events.length)
                    .toBe(1);
                return utils.check_event(events, 0, "skipped", module_name, true);
            })
    });
});

describe("One plugins", () =>
{
    test("Without config file", () =>
    {
        var events = [];
        var obj;
        var module_name = path.resolve(__dirname, "./ressources/plugin2/archiotect.js");
        obj = new Archiotect([
            {
                module_name
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(2);
                utils.check_event(events, 0, "loading", module_name, false);
                utils.check_event(events, 1, "loaded",
                {
                    exports: "plugin2",
                    module_name,
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
            });
    });

    test.each(["js", "json", "yaml"])("With config file config.%s", (ext) =>
    {
        var events = [];
        var obj;
        var module_name = path.resolve(__dirname, "./ressources/plugin2/archiotect.js");
        obj = new Archiotect([
            {
                module_name,
                config_file: path.resolve(__dirname, "./ressources/config." + ext)
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(2);
                utils.check_event(events, 0, "loading", module_name, false);
                utils.check_event(events, 1, "loaded",
                {
                    exports: undefined,
                    module_name,
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined,
                    config_file: undefined,
                    unload: undefined
                }, false);
            });
    });

    test("With unmet dependency", () =>
    {
        var events = [];
        var obj;
        var module_name = path.resolve(__dirname, "./ressources/plugin1/archiotect.js");
        obj = new Archiotect([
            {
                module_name,
                config_file: path.resolve(__dirname, "./ressources/config.js")
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() => expect("Should not have been here")
                .toBe("But did anyway"))
            .catch(() =>
            {
                expect(events.length)
                    .toBe(1);
                utils.check_event(events, 0, "loading", module_name, false);
            });
    });

    test("With wrong config file", () =>
    {
        var events = [];
        var obj;
        var module_name = path.resolve(__dirname, "./ressources/plugin2/archiotect.js");
        obj = new Archiotect([
            {
                module_name,
                config_file: path.resolve(__dirname, "./ressources/config.jse")
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() => expect("Should not have been here")
                .toBe("But did anyway"))
            .catch(() =>
            {
                expect(events.length)
                    .toBe(1);
                utils.check_event(events, 0, "loading", module_name, false);
            });
    });

    test("Whitelisted", () =>
    {
        var events = [];
        var obj;
        var module_name = path.resolve(__dirname, "./ressources/plugin2/archiotect.js");
        obj = new Archiotect([
            {
                module_name,
        }]);
        utils.setup_event(obj, events);
        return obj.load("plugin3")
            .then(() =>
            {
                expect(events.length)
                    .toBe(1);
                utils.check_event(events, 0, "skipped", module_name, true);
            });
    });
});

describe("Two plugins", () =>
{
    test("Without config", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, "./ressources/plugin1/archiotect.js"));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(4);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "loading", module_name[1], false);
                utils.check_event(events, 2, "loaded",
                {
                    exports: "plugin1",
                    module_name: module_name[1],
                    name: "plugin1",
                    version: "1.0.0",
                    need: ["plugin2"],
                    load: undefined
                }, false);
                utils.check_event(events, 3, "loaded",
                {
                    exports: "plugin2",
                    module_name: module_name[0],
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
            });
    });

    test("Whitelisting not depended upon", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, "./ressources/plugin1/archiotect.js"));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load(["plugin2"])
            .then(() =>
            {
                expect(events.length)
                    .toBe(3);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "skipped", module_name[1], true);
                utils.check_event(events, 2, "loaded",
                {
                    exports: "plugin2",
                    module_name: module_name[0],
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
            });
    });

    test("Whitelisting with string", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, "./ressources/plugin1/archiotect.js"));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load("plugin2")
            .then(() =>
            {
                expect(events.length)
                    .toBe(3);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "skipped", module_name[1], true);
                utils.check_event(events, 2, "loaded",
                {
                    exports: "plugin2",
                    module_name: module_name[0],
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
            });
    });

    test("Whitelisting depended upon", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, "./ressources/plugin1/archiotect.js"));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load(["plugin1"])
            .then(() => expect("Should not have been here")
                .toBe("But did anyway"))
            .catch(() =>
            {
                expect(events.length)
                    .toBe(2);
                utils.check_event(events, 0, "skipped", module_name[0], true);
                utils.check_event(events, 1, "loading", module_name[1], false);
            });
    });

    test("Duplicate plugin", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() => expect("Should not have been here")
                .toBe("But did anyway"))
            .catch(() =>
            {
                expect(events.length)
                    .toBe(2);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "loaded",
                {
                    exports: "plugin2",
                    module_name: module_name[1],
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined,
                }, false);
            });
    });

    test.each(["js", "json", "yaml"])("With config file config.%s", (ext) =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, "./ressources/plugin1/archiotect.js"));
        obj = new Archiotect([
            {
                module_name: module_name[0],
                config_file: path.resolve(__dirname, "./ressources/config." + ext)
        },
            {
                module_name: module_name[1],
                config_file: path.resolve(__dirname, "./ressources/config." + ext)
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(4);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "loading", module_name[1], false);
                utils.check_event(events, 2, "loaded",
                {
                    exports: "plugin1",
                    module_name: module_name[1],
                    name: "plugin1",
                    version: "1.0.0",
                    need: ["plugin2"],
                    load: undefined,
                }, false);
                utils.check_event(events, 3, "loaded",
                {
                    exports: "plugin2",
                    module_name: module_name[0],
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
            });
    });
})

describe("Unload plugin", () =>
{
    test("Wrong parameters", () =>
    {
        var events = [];
        var obj;
        var module_name = path.resolve(__dirname, "./ressources/plugin2/archiotect.js");
        obj = new Archiotect([
            {
                module_name
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(2);
                utils.check_event(events, 0, "loading", module_name, false);
                utils.check_event(events, 1, "loaded",
                {
                    exports: "plugin2",
                    module_name,
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
                return obj.unload('po');
            })
            .then((res) =>
            {
                expect(res)
                    .toEqual([]);
                expect(events.length)
                    .toBe(2);
            })
    });

    test("An unknown plugin", () =>
    {
        var events = [];
        var obj;
        var module_name = path.resolve(__dirname, "./ressources/plugin2/archiotect.js");
        obj = new Archiotect([
            {
                module_name
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(2);
                utils.check_event(events, 0, "loading", module_name, false);
                utils.check_event(events, 1, "loaded",
                {
                    exports: "plugin2",
                    module_name,
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
                return obj.unload("plugin22");
            })
            .then((res) =>
            {
                expect(res)
                    .toEqual([]);
                expect(events.length)
                    .toBe(2);
            })
    });

    test("A single plugin", () =>
    {
        var events = [];
        var obj;
        var module_name = path.resolve(__dirname, "./ressources/plugin2/archiotect.js");
        obj = new Archiotect([
            {
                module_name
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(2);
                utils.check_event(events, 0, "loading", module_name, false);
                utils.check_event(events, 1, "loaded",
                {
                    exports: "plugin2",
                    module_name,
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
                return obj.unload("plugin2");
            })
            .then((res) =>
            {
                expect(res)
                    .toEqual(["plugin2"]);
                expect(events.length)
                    .toBe(4);
                utils.check_event(events, 2, "unloading", "plugin2", false);
                utils.check_event(events, 3, "unloaded", "plugin2", false);
            })
    });

    test("Two plugins", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, "./ressources/plugin1/archiotect.js"));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(4);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "loading", module_name[1], false);
                utils.check_event(events, 2, "loaded",
                {
                    exports: "plugin1",
                    module_name: module_name[1],
                    name: "plugin1",
                    version: "1.0.0",
                    need: ["plugin2"],
                    load: undefined
                }, false);
                utils.check_event(events, 3, "loaded",
                {
                    exports: "plugin2",
                    module_name: module_name[0],
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
                return obj.unload("plugin2");
            })
            .then((res) =>
            {
                expect(res)
                    .toEqual(["plugin2"]);
                expect(events.length)
                    .toBe(6);
                utils.check_event(events, 4, "unloading", "plugin2", false);
                utils.check_event(events, 5, "unloaded", "plugin2", false);
            })
    });

    test("A depended upon plugin", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, "./ressources/plugin1/archiotect.js"));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(4);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "loading", module_name[1], false);
                utils.check_event(events, 2, "loaded",
                {
                    exports: "plugin1",
                    module_name: module_name[1],
                    name: "plugin1",
                    version: "1.0.0",
                    need: ["plugin2"],
                    load: undefined
                }, false);
                utils.check_event(events, 3, "loaded",
                {
                    exports: "plugin2",
                    module_name: module_name[0],
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
                return obj.unload("plugin1");
            })
            .then((res) =>
            {
                expect(res)
                    .toEqual(["plugin2", "plugin1"]);
                expect(events.length)
                    .toBe(8);
                utils.check_event(events, 4, "unloading", "plugin1", false);
                utils.check_event(events, 5, "unloading", "plugin2", false);
                utils.check_event(events, 6, "unloaded", "plugin2", false);
                utils.check_event(events, 7, "unloaded", "plugin1", false);
            })
    });

    test("Unload an array of plugins", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, "./ressources/plugin1/archiotect.js"));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(4);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "loading", module_name[1], false);
                utils.check_event(events, 2, "loaded",
                {
                    exports: "plugin1",
                    module_name: module_name[1],
                    name: "plugin1",
                    version: "1.0.0",
                    need: ["plugin2"],
                    load: undefined
                }, false);
                utils.check_event(events, 3, "loaded",
                {
                    exports: "plugin2",
                    module_name: module_name[0],
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
                return obj.unload(["plugin2", "plugin1"]);
            })
            .then((res) =>
            {
                expect(res)
                    .toEqual(["plugin2", "plugin1"]);
                expect(events.length)
                    .toBe(8);
                utils.check_event(events, 4, "unloading", "plugin2", false);
                utils.check_event(events, 5, "unloaded", "plugin2", false);
                utils.check_event(events, 6, "unloading", "plugin1", false);
                utils.check_event(events, 7, "unloaded", "plugin1", false);
            })
    });

    test("Unload all", () =>
    {
        var events = [];
        var obj;
        var module_name = [];
        module_name.push(path.resolve(__dirname, "./ressources/plugin2/archiotect.js"));
        module_name.push(path.resolve(__dirname, "./ressources/plugin1/archiotect.js"));
        obj = new Archiotect([
            {
                module_name: module_name[0]
        },
            {
                module_name: module_name[1]
        }]);
        utils.setup_event(obj, events);
        return obj.load()
            .then(() =>
            {
                expect(events.length)
                    .toBe(4);
                utils.check_event(events, 0, "loading", module_name[0], false);
                utils.check_event(events, 1, "loading", module_name[1], false);
                utils.check_event(events, 2, "loaded",
                {
                    exports: "plugin1",
                    module_name: module_name[1],
                    name: "plugin1",
                    version: "1.0.0",
                    need: ["plugin2"],
                    load: undefined
                }, false);
                utils.check_event(events, 3, "loaded",
                {
                    exports: "plugin2",
                    module_name: module_name[0],
                    name: "plugin2",
                    version: "1.0.0",
                    need: [],
                    load: undefined
                }, false);
                return obj.unload();
            })
            .then((res) =>
            {
                expect(res)
                    .toEqual(["plugin1", "plugin2"]);
                expect(events.length)
                    .toBe(8);
                utils.check_event(events, 4, "unloading", "plugin1", false);
                utils.check_event(events, 5, "unloading", "plugin2", false);
                utils.check_event(events, 6, "unloaded", "plugin2", false);
                utils.check_event(events, 7, "unloaded", "plugin1", false);
            })
    });
})