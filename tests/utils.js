const _ = require("lodash");

function setup_event(obj, dest)
{
    ["loading", "loaded", "skipped", "unloading", "unloaded"].forEach((ev) => obj.on(ev, (msg, error) => dest.push([ev, msg, error])));
}

function check_event(arr, pos, type, msg, should_have_error)
{
    expect(_.get(arr, [pos, 0]))
        .toBe(type);
    if (msg)
    {
        if (typeof msg === "string")
            expect(_.get(arr, [pos, 1 + should_have_error]))
            .toBe(msg);
        else
        {
            Object.keys(msg)
                .forEach((prop) =>
                {
                    expect(_.get(arr, [pos, 1 + should_have_error, prop]))
                        .toBeDefined()
                    if (_.get(msg, [prop]) === undefined)
                        return;
                    expect(_.get(arr, [pos, 1 + should_have_error, prop]))
                        .toEqual(_.get(msg, [prop]));
                })
        }
    }
    if (should_have_error)
        expect(_.get(arr, [pos, 1]))
        .toBeDefined();
}

module.exports = {
    setup_event,
    check_event
};