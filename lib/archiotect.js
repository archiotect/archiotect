"use strict";

const Joi = require("joi");
const EventEmitter = require("events");
const Promise = require("bluebird");
const toposort = require("toposort");
const _ = require("lodash");
const fs = require("fs");
const yaml = require("js-yaml");

const option_schema = {
    module_name: Joi.string()
        .required(),
    config_file: Joi.string(),
    config: Joi.any()
};

const options_schema = Joi.array()
    .items(option_schema)
    .default([]);


const plugin_schema = Joi.object()
    .keys(
    {
        name: Joi.string()
            .required(),
        version: Joi.string()
            .required()
            .regex(/\d+\.\d+\.\d+/),
        need: Joi.array()
            .items(Joi.string())
            .default([]),
        load: Joi.func()
            .required(),
        unload: Joi.func(),
        core: Joi.boolean()
            .default(false),
        handle: Joi.func()
            .when("core",
            {
                is: true,
                then: Joi.required(),
                otherwise: Joi.forbidden()
            }),
        exports: Joi.forbidden()
    })
    .unknown(true)
    .required();

/**
 * Get plugins config (JSON, JS or YAML)
 *
 * @param {String} path The config path
 * @returns {Object} The config object
 */
function get_config(path)
{
    var file_ext;

    file_ext = ((/[.]/.exec(path)) ? /[^.]+$/.exec(path) : undefined);

    if (!path)
        return Promise.resolve(
        {});
    switch (file_ext[0])
    {
        case "js":
        case "ts":
        case "json":
            return Promise.resolve(require(path));
        case "yaml":
            return Promise.resolve(yaml.safeLoad(fs.readFileSync(path,
            {
                encoding: "utf8"
            }),
            {
                filename: path
            }));
        default:
            return Promise.reject(new Error(`Extension '${file_ext}' for config
             is not authorized`));
    }
}
class Archiotect extends EventEmitter
{
    constructor(options)
    {
        super();
        this._options = {};
        this._plugins = {};
        this._cancel = Symbol("Cancel");
        var validation;
        validation = Joi.validate(options, options_schema);
        if (validation.error) //Check if options respect schema, throw if not
            throw validation.error;
        this._options = validation.value;
    }

    /**
     * Create the list of plugins and fetching plugins metadata
     *
     * @param {Array<String>|undefined} whitelist A list of plugins to load, all plugins if unspecified
     * @returns {Array<Object>} The unsorted/unloaded plugins list
     * @memberof Archiotect
     */
    _create_unsorted_plugin_list(whitelist)
    {
        var unsorted_plugins = [];

        return Promise.each(this._options, (plugin) =>
            {
                return Promise.resolve(require(plugin.module_name))
                    .then((req) => Object.assign(plugin, req))
                    .then((req) =>
                    {
                        let index;

                        if ((index = _.findIndex(unsorted_plugins,
                            {
                                name: req.name
                            })) >= 0)
                            throw new Error(`Trying to add 2 plugin with the
                            same name : '${req.module_name}' and
                            '${unsorted_plugins[index].module_name}'`);
                        return req;
                    })
                    .then((req) =>
                    {
                        let val = Joi.validate(req, plugin_schema.append(option_schema));

                        if (val.error ||
                            (whitelist.length && whitelist.indexOf(req.name) < 0))
                            return this.emit("skipped", val.error, req.module_name);
                        unsorted_plugins.push(val.value)
                        this.emit("loading", plugin.module_name);
                    })
                    .catch((err) => this.emit("loading_failed", err, plugin.module_name))
            })
            .then(() => _.orderBy(unsorted_plugins, "core", "asc"))
    }

    /**
     * Create a sorted array of plugins to satisfy all dependencies
     *
     * @param {Array<Object>} unsorted_plugins An unsorted and unloaded plugins array
     * @returns {Array<Object>} The sorted and unloaed array of plugins
     * @memberof Archiotect
     */
    _resolve_plugins_dependencies(unsorted_plugins)
    {
        var graph = [];
        var result;
        var sorted_plugins = [];

        unsorted_plugins.forEach((plugin) =>
        {
            plugin.need.forEach((need) => graph.push([need, plugin.name]));
        });
        result = toposort(graph);
        result.forEach((plugin) =>
        {
            if (_.findIndex(unsorted_plugins,
                {
                    name: plugin
                }) < 0)
                throw new Error(`Plugin ${plugin} is depended upon but doesn't exists`);
        })
        unsorted_plugins.forEach((plugin) =>
        {
            let index = result.indexOf(plugin.name);
            if (index < 0)
                result.push(plugin.name);
        });
        result.forEach((plugin) =>
        {
            sorted_plugins.push(unsorted_plugins.splice(unsorted_plugins.indexOf(plugin), 1)[0]);
        });
        return sorted_plugins;
    }

    /**
     * Run handle in core plugin
     *
     * @param {Object} plugin The object that is being treated
     * @param {String} exec_time The time of which this function is being call (can be 'pre_load' or 'post_load')
     * @returns {Promise} A promise upon compeletion of all the handles
     * @memberof Archiotect
     */
    _run_core_plugins(plugin, exec_time)
    {
        return Promise.each(Object.keys(this._plugins), (key) =>
        {
            if (this._plugins[key].core)
                return Promise.resolve(this._plugins[key].handle(exec_time, plugin, this))
                    .then((res) =>
                    {
                        if (res)
                        {
                            this.emit("skipped", res, plugin.module_name);
                            throw this._cancel;
                        }
                        return !res;
                    })
        });
    }

    /**
     * Add plugins to the class
     *
     * @param {Array<Object>} sorted_plugins A sorted (by dependencies) array of plugin to load
     * @returns {Array<Object>} A sorted (by dependencies) array of loaded plugin
     * @memberof Archiotect
     */
    _add_plugins(sorted_plugins)
    {
        return Promise.each(sorted_plugins, (plugin, index) => get_config(plugin.config_file)
                .then((res) => Object.assign(plugin.config ||
                {}, res ||
                {}))
                .then(() => this._run_core_plugins(plugin, "pre_load"))
                .then(() => plugin.load(plugin.config, this._plugins, plugin.core ? this : undefined))
                .then((res) => plugin.exports = res)
                .then(() => this._run_core_plugins(plugin, "post_load"))
                .then(() => this._plugins[plugin.name] = plugin)
                .then(() => this.emit("loaded", this._plugins[plugin.name]))
                .catch((err) =>
                {
                    if (err === this._cancel)
                        return sorted_plugins.splice(index, 1);
                    throw err;
                }))
            .return(sorted_plugins);
    }

    /**
     * Load a/multiple plugin(s)
     *
     * @param {Array<String>|undefined} whitelist An array of all the plugin to load or undefined to load them all
     * @returns {Object} An object containing all the loaded plugins
     * @memberof Archiotect
     */
    load(whitelist)
    {
        var nwhitelist = whitelist;

        if (_.isString(whitelist))
            nwhitelist = [whitelist];
        else if (!_.isArray(whitelist))
            nwhitelist = [];
        return this._create_unsorted_plugin_list(nwhitelist)
            .then((res) => this._resolve_plugins_dependencies(res))
            .then((sorted_plugins) => this._add_plugins(sorted_plugins))
    }

    /**
     * Unload a/multiple plugin(s)
     *
     * @param {Array<String>|undefined} plugin An array of all the plugin to unload or undefined to unload them all
     * @returns {Array<String>} An array of all unloaded plugins (in order of unload)
     * @memberof Archiotect
     */
    unload(plugin)
    {
        if (_.isString(plugin) && this._plugins[plugin])
        {
            this.emit("unloading", plugin);
            return new Promise((resolve) =>
                {
                    if (this._plugins[plugin].need.length)
                        return resolve(Promise.each(this._plugins[plugin].need, (need) => this.unload(need)));
                    resolve([]);
                })
                .then((list) => Promise.resolve(this._plugins[plugin].unload ? this._plugins[plugin].unload() : true)
                    .then(() => delete this._plugins[plugin])
                    .then(() => this.emit("unloaded", plugin))
                    .return([...list, plugin]));
        }
        else if (_.isArray(plugin))
            return Promise.each(plugin, (plugin_name) => this.unload(plugin_name));
        else if (_.isUndefined(plugin))
            return Promise.each(Object.keys(this._plugins), (plugin_name) => this.unload(plugin_name));
        else
            return Promise.resolve([]);
    }

    get plugins()
    {
        return this._plugins;
    }

    set plugins(val)
    {
        return val;
    }
}

module.exports = Archiotect;