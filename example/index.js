const Archiotect = require("./../lib/archiotect");
const path = require("path");

const example_app = new Archiotect(
[
    {
        module_name: path.resolve(__dirname, "./archiotect_config/lib/archiotect_config")
    },
    {
        module_name: path.resolve(__dirname, "./archiotect_logger/lib/archiotect_logger")
    }
]);

example_app.on("loading", (name, err) => console.log("Loading :", name));
example_app.on("loading_failed", (name, err) => console.log("Loading failed :", name, err));
example_app.on("loaded", (plugins) => console.log("Loaded :", plugins.name));
example_app.on("skipped", (name) => console.log("Loading skipped :", name));
example_app.on("unloading", (name) => console.log("Unloading :", name));
example_app.on("unloaded", (name) => console.log("Unloaded :", name));

example_app.load()
    .then((plugins) =>
    {
        console.log(`${"-".repeat(10)}Plugins loaded successfully${"-".repeat(10)}`);
        console.log(example_app.plugins);
    })
    .then(() => example_app.unload())
    .then(() => console.log(`${"-".repeat(10)}Plugin unloaded${"-".repeat(10)}`))