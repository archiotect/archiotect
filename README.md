![pipeline](https://gitlab.com/archiotect/archiotect/badges/master/build.svg "Build status")
![coverage](https://gitlab.com/archiotect/archiotect/badges/master/coverage.svg "Code coverage")


# Archiotect

This project aim to bring a usable plugin loader without putting to much constraint on the developper. Creating a plugin
is dead simple. One juste need to declare a formatted config script (see below) and import that script when declaring
the Archiotect. One should only need one Archiotect instance.

## Usage

```javascript
const Archiotect = require("./lib/archiotect");
var AIO = new Archiotect([
    {
        module_name: "my_requirable_module",
        config_file: path.resolve(__dirname, "config.json") //Can be JSON, js, or YAML
    },
]);
AIO.on("loading", (name) => console.log("Loading :", module_name));
AIO.on("loading_failed", (err, name) => console.log("Loading failed :", name, err));
AIO.on("loaded", (plugin) => console.log("Loaded :", plugin.name));
AIO.on("skipped", (name) => console.log("Loading skipped :", module_name));
AIO.on("unloading", (name) => console.log("Unloading :", name));
AIO.on("unloaded", (name) => console.log("Unloaded :", name));

AIO.load() //Can pass an array of string to filter which plugins to load
    .then((res) => console.log(res)) // Object containing all the loaded plugins
    .catch((err) => console.trace(err))
    .then(() => AIO.unload("my_plugin_name")); // Unload one or multiple plugins, accepts an array, a string or nothing.
                                               // Unload all if no arguments
```

## Defining a plugin

It must expose an `handle` function.
This function will be call before and after the plugin has beed loaded. If the function return a truthable value, the
loading process for this plugin will be discarded and a `skipped` event will be fired.

```javascript
module.exports = {
    name: "plugin0", // required, the plugin name. This is used as key in the final plugin object
    version: "1.0.0", // required, for version verification
    need: ["plugin1", "plugin2"], // optional, name of plugin that are need for this one
    load: (config, plugins) => { // required, function called to load the plugin. This can return a promise. The return
                                // value will be accessible in the plugins[name].exports by other plugins loaded after
        return "plugin";
    },
    unload: () => //optional, will when this plugin is unloaded
    {
        return Promise.resolve(undefined);
    }
}
```

## Defining a core plugin

A core plugin works the same as a normal plugin but aim to modify the way other plugins will be loaded hereafter. 

```javascript
module.exports = {
    name: "plugin0",
    version: "1.0.0",
    need: ["plugin1", "plugin2"],
    load: (config, plugins) => {
        return "plugin";
    },
    core: true, //specify it's a core plugin
    handle: (exec_time, plugin, archiotect_instance) => { // the first argument can be "pre_load" or "post_load".
                                                          // the third argument is the instance of archiotect
        if (exec_time === "pre_load")
            return false; // return a falsy value to let the loading process continue
        else if (exec_time === "post_load")
            return true;
    },
    unload: () =>
    {
        return Promise.resolve(undefined);
    }
}
```